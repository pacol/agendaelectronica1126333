
package agenda;
import java.util.*;
import java.util.Scanner;

public abstract class persona {
    public String nombre;
    public String apellido;
    public String edad;
    public String correo;
    public String sexo;
    public String documento;

    
    public String getNombre() {
        return nombre;
    }

    
    public void setNombre(String nombre) {
        this.nombre = nombre;
    }

    
    public String getApellido() {
        return apellido;
    }

    
    public void setApellido(String apellido) {
        this.apellido = apellido;
    }

    
    public String getEdad() {
        return edad;
    }

  
    public void setEdad(String edad) {
        this.edad = edad;
    }

   
    public String getCorreo() {
        return correo;
    }

   
    public void setCorreo(String correo) {
        this.correo = correo;
    }

    
    public String getSexo() {
        return sexo;
    }

   
    public void setSexo(String sexo) {
        this.sexo = sexo;
    }

    
    public String getDocumento() {
        
        return documento;
    }

    
    public void setDocumento(String documento) {
        this.documento = documento;
    }
    //declarar metodos
    public void registrar (){};
    public void modificar (){};
    public void borrar(){};
    
    
    public static void main (String [] args){
        Scanner le = new Scanner (System.in);
        String opcion="";
        Agenda A = new Agenda ();
     
        
        
        do {
            System.out.println("Agenda Electronica");
            System.out.println("1 para registrar");
            System.out.println("2 para modificar");
            System.out.println("3 para borrar");
            System.out.println("Digite la opcion a realizar");
            opcion = le.next();
            switch (opcion){
            case "1": 
                A.registrar();
                break;
                    
            case "2": 
                A.modificar();
                break;
            
            
            case "3": 
                A.borrar();
                break;
            
            
            default: break;
        }
        } while (opcion!="SALIR");
        
    }
    
    
}
