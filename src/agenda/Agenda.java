
package agenda;

import java.util.Scanner;

public class Agenda extends persona{
    //declarar variables
    private String respuesta;
    
    //matriz 
    String [] [] M = new String [2] [6];
    public void registrar(){
        Scanner agenda = new Scanner(System.in); 
     
            //se generan los registros de la matriz
             for (int filas = 0; filas < 2; filas++) { //lee el numero de filas
                  for (int columna = 0; columna < 6; columna++) { //lee el numero de columnas
                      if (columna==0) {
                        System.out.print("Digite el nombre: ");
                        String nom = agenda.nextLine();
                        nombre=nom;
                        M[filas][columna] = nombre;
                    }
                    else{
                        if (columna==1) {
                            System.out.print("Digite el apellido: ");
                            String apell = agenda.nextLine();
                            apellido=apell;
                            M[filas][columna] = apellido;
                        }
                        else{
                        if (columna==2) {
                            System.out.print("Digite la edad: ");
                            String ed = agenda.nextLine();
                            edad=ed;
                            M[filas][columna] = edad;
                        }
                        else{
                        if (columna==3) {
                            System.out.print("Digite el correo: ");
                            String cor = agenda.nextLine();
                            correo=cor;
                            M[filas][columna] = correo;
                        }
                        
                        else{
                        if (columna==4) {
                            System.out.print("Digite el sexo (M si es hombre, F si es mujer, MK si es indefinido): ");
                            String sex = agenda.nextLine();
                            sexo=sex;
                            M[filas][columna] = sexo;
                        }
                        
                        else{
                        if (columna==5) {
                            System.out.print("Digite el documento: ");
                            String doc = agenda.nextLine();
                            documento=doc;
                            M[filas][columna] = documento;
                            
                            
                        }
                        
                            
                  
                        }
                                 
                        }
        
                        }
        
                        }
                      
                      }
 
                  }
             }
        
       //recorremos la matriz y muestra los valores 
        for (int filas = 0; filas < 2; filas++) {
            for (int columna = 0; columna < 6; columna++) {
                    if(columna==0){
                        //codigo para cambiar dato a mayuscula
                        String l1=M[filas][columna];
                        System.out.print("Mayuscula:"+l1.toUpperCase()+"/");
                        //codigo para cambiar dato a minuscula
                        String minusculas=M[filas][columna];
                        System.out.print("Minuscula:"+minusculas.toLowerCase()+" ");
                        //codigo para saber cantidad de caracteres
                        int longitud = l1.length();
                        System.out.print(" = "+longitud+ " - ");
                    }
                    else{
                        if(columna==1){
                            int longitud = M[filas][columna].length();
                            System.out.print(M[filas][columna]+"  ");
                        System.out.print(" = "+longitud+" - ");
                    }
                        else{
                            if(columna==2){
                         int longitud = M[filas][columna].length();
                        System.out.print(M[filas][columna]+"  ");
                        System.out.print(" = "+longitud+" - ");
                    }
                            else{
                                if(columna==3){
                         int longitud = M[filas][columna].length();
                        System.out.print(M[filas][columna]+"  ");
                        System.out.print(" = "+longitud+" - ");
                    }
                         else{
                           if(columna==4){
                         int longitud = M[filas][columna].length();
                         System.out.print(M[filas][columna]+"  ");
                         System.out.print(" = "+longitud+" - ");
                    }                
                        else{
                         if(columna==5){
                          int longitud = M[filas][columna].length();
                           System.out.print(M[filas][columna]+"  ");
                          System.out.print(" = "+longitud+"  ");
                        System.out.println(" ");
                        
                    }
                                }
                            }
                        }
                    }
                }
            }
        }
    }
           //metodo modificar 
    @Override
         public void modificar(){ 
        Scanner leer = new Scanner(System.in);
        
            //Se genera la busqueda del respectivo campo a actualizar en la matriz
            System.out.println("Ingrese la coordenada "); 
            System.out.print("Fila: ");
            int f=leer.nextInt();
            System.out.print("Columna: ");
            int c=leer.nextInt();
            System.out.println("Digite el dato:");
            String datoA = leer.nextLine();
            M[f][c]=datoA;
            
            //vuelve y recorre la matriz y muestra los datos con la actualizacion
             for (int filas = 0; filas < 2; filas++) {
            for (int columna = 0; columna < 6; columna++) {
                    if(columna==0){
                         //codigo para cambiar dato a mayuscula
                        String l1=M[filas][columna];
                        System.out.print("Mayuscula:"+l1.toUpperCase()+"/");
                      //codigo para cambiar dato a minuscula
                        String minusculas=M[filas][columna];
                        System.out.print("Minuscula:"+minusculas.toLowerCase()+" ");
                           //codigo para saber cantidad de caracteres
                        int longitud = l1.length();
                        System.out.print(" = "+longitud+ " - ");
                    }
                    else{
                        if(columna==1){
                         int longitud = M[filas][columna].length();
                          System.out.print(M[filas][columna]+"  ");
                        System.out.print(" = "+longitud+" - ");
                    }
                        else{
                            if(columna==2){
                         int longitud = M[filas][columna].length();
                         System.out.print(M[filas][columna]+"  ");
                        System.out.print(" = "+longitud+" - ");
                    }
                            else{
                                if(columna==3){
                        int longitud = M[filas][columna].length();
                        System.out.print(M[filas][columna]+"  ");
                        System.out.print(" = "+longitud+" - ");
                    }
                                else{
                        if(columna==4){
                        int longitud = M[filas][columna].length();
                        System.out.print(M[filas][columna]+"  ");
                        System.out.print(" = "+longitud+" - ");
                    }                
                         else{
                            if(columna==5){
                          int longitud = M[filas][columna].length();
                          System.out.print(M[filas][columna]+"  ");
                         System.out.print(" = "+longitud+"  ");
                         System.out.println(" ");
                    }
                                    
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
    
}
      //metodo borrar 
    @Override
      public void borrar(){  
        Scanner leer = new Scanner(System.in);
        
            System.out.println("Ingrese la coordenada "); 
            System.out.print("Fila: ");
            int f=leer.nextInt();
            System.out.print("Columna: ");
            int c=leer.nextInt();
            String datoE="  ";
            M[f][c]=datoE;   
            
             //vuelve y recorre la matriz y muestra los datos con las actualizaciones y los espacios q se borraron 
             for (int filas = 0; filas < 2; filas++) {
            for (int columna = 0; columna < 6; columna++) {
                    if(columna==0){
                        //codigo para cambiar dato a mayuscula
                        String l1=M[filas][columna];
                        System.out.print("Mayuscula:"+l1.toUpperCase()+"/");
                        //codigo para cambiar dato a minuscula
                        String minusculas=M[filas][columna];
                        System.out.print("Minuscula:"+minusculas.toLowerCase()+" ");
                           //codigo para saber cantidad de caracteres
                        int longitud = l1.length();
                        System.out.print(" = "+longitud+ " - ");
                     }
                    else{
                        if(columna==1){
                         int longitud = M[filas][columna].length();
                         System.out.print(M[filas][columna]+"  ");
                         System.out.print(" = "+longitud+" - ");
                    }
                        else{
                            if(columna==2){
                            int longitud = M[filas][columna].length();
                            System.out.print(M[filas][columna]+"  ");
                            System.out.print(" = "+longitud+" - ");
                    }  
                            else{
                                if(columna==3){
                                int longitud = M[filas][columna].length();
                                System.out.print(M[filas][columna]+"  ");
                                System.out.print(" = "+longitud+" - ");
                    }
                                else{
                                    if(columna==4){
                                   int longitud = M[filas][columna].length();
                                   System.out.print(M[filas][columna]+"  ");
                                   System.out.print(" = "+longitud+" - ");
                    }                
                                     else{
                                    if(columna==5){
                                 int longitud = M[filas][columna].length();
                                 System.out.print(M[filas][columna]+"  ");
                                 System.out.print(" = "+longitud+"  ");
                                     System.out.println(" ");
                    }
                                    
                                    
                                }
                            }
                        }
                    }
                }
            }
        }
    
}
}   
         
    






    

